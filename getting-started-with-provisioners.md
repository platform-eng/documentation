# Documentation

  You can use Terraform Provisioners to create resources of various services using YAML. This approach lets you deploy multiple resources instantly , also you can precisely apply same configuration multiple times. You can also keep track of changes based on which template is being applied . 

## Getting started

  ### &nbsp;Steps to follow for temporary resource provision from local device :

  _**Prequisites :**_ 
  -  [`aws cli` ](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)
  - configure [`aws`](https://docs.aws.amazon.com/cli/latest/reference/configure/) account keys. 
  - [`yq`](https://mikefarah.gitbook.io/yq/v/v3.x/#install)
  - [`jq`](https://jqlang.github.io/jq/download/)
  - [`terraform`](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
  - [`curl`](https://curl.se/download.html)
  - [`git`](https://git-scm.com/downloads)



  1. Create a empty Folder and Download   [`terraswifft`](https://terraswifft.gitlab.io/website/main/downloads/terraswifft-cli/1.0.0/terraswifft.sh)  in that folder.
      ```
        wget -O terraswifft.sh https://terraswifft.gitlab.io/website/main/downloads/terraswifft-cli/1.0.0/terraswifft.sh

      ```

  2. Inside folder create YAML configuration file. 

      ### Example Configuration File for VPC 

      ```yaml
        backend_config:
          backend: s3  # Accepts  's3' and 'local'
          bucket: <bucket_name>
          key: <state_file_path>
          region: <region>
          dynamodb_table: <dyanamoDB_table_name>

        common_config:
          allowed_account_ids:
            - <account_id>
          region: <region>
          default_tags:
            <key1>: <value>
            <key2>: <value>

        vpc:
          test-vpc:
            cidr: 10.0.0.0/16
            azs:
              - "us-west-1a"
              - "us-west-1b"
            public_subnets:
              - 10.0.1.0/24
              - 10.0.2.0/24
            public_subnet_names:
              - "starter-public-subnet-1"
              - "starter-public-subnet-2"
            map_public_ip_on_launch: true

      ``` 
      As shown in the above example "common_config" and "backend_config" are mandatory for provisioning any resource.

      - If you want don't want to store terraform state files on  " S3 " you can set " backend: local " in "backend_config" block.
      ```yaml
        backend_config:
          backend: local

        common_config:
          ...
      ```
      **Important Instructions** : 
        - Bucket and Dynamo DB table specified in Configuration File should be accurate, Otherwise `terraswifft` will create Bucket and Dynamodb Table with names specified in file.
        - Dynamodb Table should have "LockId" as partition key , otherwise `terraswifft` will throw an error.
  3. Run `terraswifft.sh` file with required options.
      ```
      ./terraswifft.sh -c <component_name> -v <version> -o <operation> -f <file_path> 
      ```
      - -c  &nbsp;&nbsp; Component Name like vpc , s3 , s3_cloudfront ,etc. (only lowercase).
      - -v  &nbsp;&nbsp; Component Version ex. 1.0.0 
      - -o  &nbsp;&nbsp; Operation , Accepts `plan` , `apply` , `destroy` , `plan-destroy` , `validate` , `show` , `output` .
      - -f  &nbsp;&nbsp; Path of configuration file.

      Example : I have a 'vpc.yaml' file which has configuration to create vpc. To create resources command will be :
      ```
      ./terraswifft.sh -c vpc -v 1.0.0 -o plan -f vpc.yaml
      ```

  <br>

### Steps to follow for creating pipelines using gitlab-ci :

1. You will have to create different repositories for different provisioners.
2. Repository should contain your YAML configuration file and Pipeline file i.e. .gitlab-ci.yml. Configuration file should be written using above syntax. While writing configuration file if you keep "backend: local" then state files will not be stored on a remote location.

3. Also create `.gitlab-ci.yml` .

- If you want to use our generic template then you need to include the path `platform-eng/gitlab-ci-templates/public/platform` and filename `terraform-deployment-1.yaml`.

- While using generic template , configuration filename must follow a pattern like `{env}.terraform.tfvars.yaml` where `{env}` can be   `labs/dev/qa/staging/pt/prod`. 
ex. `dev.terraform.tfvars.yaml`

- Need to give values for these variables: <br>
  `PROVISIONER_NAME` - Name of provisioner (lowercase) <br>
  `PROVISIONER_VERSION` - Version of Provisioner (can be found in TAGS of Repository for Provisioner).<br>
  `CONFIRM_DESTROY` - This variable is required whenever you are trying destroy resources.  Default value is "false".<br>

  **NOTE: If you want to use your own runner instead of the shared, you will have to provide the variable `RUNNER_TAG` with your runner tag.
  It is optional if you do not provide `RUNNER_TAG` it will run using the shared runners.**

  ### EXAMPLE:

  #### `.gitlab-ci.yml`

  ```yaml
  include:
  - project: terraswifft/platform/gitlab-ci-templates/public/platform
    file: terraform-deployment-1.yaml


  variables:
    PROVISIONER_NAME: dynamodb
    PROVISIONER_VERSION: 1.0.0
    CONFIRM_DESTROY: "true"
    RUNNER_TAG: test

  ```
  For reference you can take a look at [`Starters`](https://gitlab.com/terraswifft/platform/starter).
---

#### All the Provisioners can be found [`here`](https://gitlab.com/terraswifft/platform/provisioners).

